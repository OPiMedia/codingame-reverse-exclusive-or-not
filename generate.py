#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Random
"""

import sys
import random


def main() -> None:
    n = (int(sys.argv[1]) if len(sys.argv) > 1
         else 10)
    max_length = (int(sys.argv[2]) if len(sys.argv) > 2
                  else 50)

    print(n)
    random.seed()

    for _ in range(n):
        print(''.join(random.choice('01')
                      for _ in range(random.randint(1, max_length))))


if __name__ == '__main__':
    main()
