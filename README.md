# Reverse exclusive, or not? (CodinGame, January 3, 2021)

Reverse Clash of code for CodinGame:
https://www.codingame.com/contribute/view/602720959783b194130c6e06928abe626ab4



## Statement
```
<<Input:>>
8
01010010
00
010001010010
10
0
11
01
1
```

```
<<Output:>>
1
0
0
1
0
0
1
1
```



## Inputs
```
<<Line 1:>> Positive integer [[nb]]

<<Next [[nb]] lines:>> Non empty string [[s]] containing only "0" and "1" characters
```



## Outputs
```
<<[[nb]] lines:>> 0 or 1
```



## Constraints
```
1 ≤ [[nb]] ≤ 100
1 ≤ length of each [[s]] ≤ 100
```



## Sub generator
```
read nb:int
loop nb read s:string(100)

write answer
```



## Link
* [**HackerRank [CodinGame…] / helpers**](https://bitbucket.org/OPiMedia/hackerrank-codingame-helpers/):
  little scripts to help in solving problems of HackerRank website (or CodinGame…)



## Author: 🌳 Olivier Pirson — OPi ![OPi][OPi] 🇧🇪🇫🇷🇬🇧 🐧 👨‍💻 👨‍🔬

🌐 Website: <http://www.opimedia.be/>

💾 Bitbucket: <https://bitbucket.org/OPiMedia/>

- 📧 <olivier.pirson.opi@gmail.com>
- Mastodon: <https://mamot.fr/@OPiMedia> — Twitter: <https://twitter.com/OPirson>
- 👨‍💻 LinkedIn: https://www.linkedin.com/in/olivierpirson/ — CV: http://www.opimedia.be/CV/English.html
- other profiles: <http://www.opimedia.be/about/>

[OPi]: http://www.opimedia.be/_png/OPi.png
