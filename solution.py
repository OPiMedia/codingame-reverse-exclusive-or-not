#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Reverse exclusive, or not?

Reverse Clash of code for CodinGame:
https://www.codingame.com/contribute/view/602720959783b194130c6e06928abe626ab4
"""


def main() -> None:
    nb = int(input())

    assert 1 <= nb <= 100

    for _ in range(nb):
        s = input()

        assert 1 <= len(s) <= 100
        assert s.count('0') + s.count('1') == len(s)

        b = False
        for c in s:
            b ^= (c == '1')

        assert b == (s.count('1') % 2 != 0)

        print('1' if b
              else '0')


if __name__ == '__main__':
    main()
